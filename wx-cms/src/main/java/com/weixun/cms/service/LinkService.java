package com.weixun.cms.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

public class LinkService {
    /**
     * 分页
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Record> paginate(int pageNumber, int pageSize,String link_title) {
        StringBuffer  sqlstr = new StringBuffer();
        sqlstr.append("from cms_link s  where 1=1 ");
        if (link_title != null && !link_title.equals("")) {
            sqlstr.append("and s.link_title like '%" + link_title + "%' ");
        }
        sqlstr.append(" order by s.link_pk desc ");

        String select = "select *";

        return Db.paginate(pageNumber, pageSize, select, sqlstr.toString());
//        return Db.paginate(pageNumber, pageSize, "select *", "from cms_link order by link_pk desc");
    }


    /**
     * 获取列表
     * @return
     */
    public List<Record> findList(String link_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from cms_link where 1=1 ");
        if (link_pk != null && !link_pk.equals(""))
        {
            stringBuffer.append("and link_pk = "+link_pk);
        }
        List<Record> list = Db.use("datasource").find(stringBuffer.toString());
        return list;
    }

    /**
     * 删除，可以根据多数据源删除
     * @param ids
     * @return
     */
    public int deleteById(String ids) {
        String sql = "delete from cms_link where link_pk in ("+ids+")";
//        Db.delete("sys_user","user_pk",ids);
        Integer result = Db.use("datasource").update(sql);
        return result;
    }
}
