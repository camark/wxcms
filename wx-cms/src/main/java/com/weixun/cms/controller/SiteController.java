package com.weixun.cms.controller;

import com.jfinal.json.JFinalJson;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.cms.service.SiteService;
import com.weixun.comm.controller.BaseController;
import com.weixun.model.SysSite;
import com.weixun.utils.ajax.AjaxMsg;

import java.util.List;

public class SiteController extends BaseController {

    SiteService siteService = new SiteService();
    /**
     * 基于layui的分页
     */
    public void pages(){
        int pageNumber = getParaToInt("page");
        int pageSize = getParaToInt("limit");
        String site_name = getPara("site_name");
        Page<Record> list = siteService.paginate(pageNumber,pageSize,site_name);//获得用户信息
        renderPageForLayUI(list);
    }

    /**
     * 查询数据列表
     */
    public void list()
    {
        String site_pk = getPara("site_pk");
        List<Record> records = siteService.findList(site_pk);
        renderJson(JFinalJson.getJson().toJson(records));
    }


    /**
     * 删除数据
     */
    public void delete()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        String site_pk = this.getPara("site_pk");
        int res =siteService.deleteById(site_pk);
        if (res >0) {
            ajaxMsg.setState("success");
            ajaxMsg.setMsg("删除成功");
        }
        else
        {
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("删除失败");
        }

        renderJson(ajaxMsg);
    }

    /**
     * 保存方法
     */
    public void saveOrUpdate()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;

        try {

            SysSite site = getModel(SysSite.class,"");
            if (site.getSitePk() != null && !site.getSitePk().equals(""))
            {
                //更新方法
                res = site.update();
            }
            else {
                //保存方法
                res = site.save();
            }
            if(res)
            {
                ajaxMsg.setState("success");
                ajaxMsg.setMsg("保存成功");
            }
            else
            {
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("保存失败");
            }
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("保存失败");
        }
        renderJson(ajaxMsg);
    }
    
}
