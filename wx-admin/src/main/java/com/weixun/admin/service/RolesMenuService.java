package com.weixun.admin.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.admin.model.vo.RolesMenu;

public class RolesMenuService {
    /**
     * 删除，可以根据多数据源删除
     * @param ids
     * @return
     */
    public int deleteByIds(String ids) {
        String sql = "delete from sys_rolesmenu where fk_roles_pk IN ('"+ids+"')";
//        Db.delete("sys_user","user_pk",ids);
        Integer result = Db.use("datasource").update(sql);
        return result;
    }


    public boolean save(RolesMenu rolesMenu)
    {
        boolean res = false;
        try {


            Record record = new Record();
            record.set("fk_roles_pk", rolesMenu.getFk_roles_pk());
            record.set("fk_menu_pk", rolesMenu.getFk_menu_pk());
            record.set("checked", rolesMenu.isChecked());
            res = Db.save("sys_rolesmenu", record);
        }
        catch (Exception e)
        {
            e.getStackTrace();
        }
        return res;
    }
}
