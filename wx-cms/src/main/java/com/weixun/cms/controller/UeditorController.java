package com.weixun.cms.controller;

import com.baidu.ueditor.ActionEnter;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.weixun.cms.service.SiteService;
import com.weixun.utils.ajax.AjaxMsg;
import com.weixun.utils.file.FileUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class UeditorController extends Controller {
    SiteService siteService = new SiteService();

    /**
     * ueditor初始化方法
     */
    public void index() {
//        String outText = ActionEnter.me().exec(getRequest());
        String rootPath = getRequest().getSession().getServletContext().getRealPath(File.separator+"static"+File.separator+"plugins"+File.separator);
        String outText = new ActionEnter(getRequest(),rootPath).exec();
        renderHtml(outText);
    }


    /**
     * 获取存储到数据库中的路径
     * 没有则手动指定路径
     * @return
     */
    private Record  getpath()
    {
        List<Record>  records =siteService.findList("");
        return records.get(0);
    }

    /**
     * 图片上传方法
     */
    public void upimage()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        Ret ret = null;
        try {
            //设置文件上传子目录
            String path="uploads/images/";
            //获取上传的文件
            UploadFile upload = getFile("upfile",path);
            File file = upload.getFile();
            //获取文件名
            String extName = FileUtils.getFileExt(file.getName());
            //获取文件上传的父目录
            String filePath = upload.getUploadPath();
            //时间命名文件
            String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + extName;
            //重命名原来的文件
            file.renameTo(new File(filePath+fileName));
            //重新组合文件路径  ip+文件目录+文件名
            String fileUrl=getpath().getStr("site_domain")+File.separator+path+fileName;

//            ajaxMsg.setState("success");
//            ajaxMsg.setMsg(fileUrl);
            ret = Ret.create("state", "SUCCESS")
                    .set("url", fileUrl)
                    .set("path",fileUrl)
                    .set("original", "")
                    .set("title",fileName);

        } catch (Exception e) {
            e.printStackTrace();
            ajaxMsg.setState("fail");
        }

        renderJson(ret);
    }

    /**
     * 视频上传方法
     */
    public void upvideo()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        Ret ret = null;
        try {
            //设置文件上传子目录
            String path="uploads/videos/";
            //获取上传的文件
            UploadFile upload = getFile("upfile",path);
            File file = upload.getFile();
            //获取文件名
            String extName = FileUtils.getFileExt(file.getName());
            //获取文件上传的父目录
            String filePath = upload.getUploadPath();
            //时间命名文件
            String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + extName;
            //重命名原来的文件
            file.renameTo(new File(filePath+fileName));
            //重新组合文件路径  ip+文件目录+文件名
            String fileUrl=getpath().getStr("site_domain")+File.separator+path+fileName;

//            ajaxMsg.setState("success");
//            ajaxMsg.setMsg(fileUrl);
            ret = Ret.create("state", "SUCCESS")
                    .set("url", fileUrl)
                    .set("path",fileUrl)
                    .set("original", "")
                    .set("title",fileName);

        } catch (Exception e) {
            e.printStackTrace();
            ajaxMsg.setState("fail");
        }

        renderJson(ret);
    }

    /**
     * 文件上传方法
     */
    public void upfile()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        Ret ret = null;
        try {
            //设置文件上传子目录
            String path="uploads/files/";
            //获取上传的文件
            UploadFile upload = getFile("upfile",path);
            File file = upload.getFile();
            //获取文件名
            String extName = FileUtils.getFileExt(file.getName());
            //获取文件上传的父目录
            String filePath = upload.getUploadPath();
            //时间命名文件
            String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + extName;
            //重命名原来的文件
            file.renameTo(new File(filePath+fileName));
            //重新组合文件路径  ip+文件目录+文件名
            String fileUrl=getpath().getStr("site_domain")+File.separator+path+fileName;

//            ajaxMsg.setState("success");
//            ajaxMsg.setMsg(fileUrl);
             ret = Ret.create("state", "SUCCESS")
                    .set("url", fileUrl)
                    .set("path",fileUrl)
                    .set("original", "")
                    .set("title",fileName);

        } catch (Exception e) {
            e.printStackTrace();
            ajaxMsg.setState("fail");
        }

        renderJson(ret);
    }


    /**
     * ueditor上传
     */
    public void config(){
        if ("config".equals(getPara("action"))) {
            render("/static/plugins/ueditor/config.json");
            return;
        }
        UploadFile file = getFile("upfile");
        String fileName = file.getFileName();
        String[] typeArr = fileName.split("\\.");
        String orig = file.getOriginalFileName();
        long size = file.getFile().length();
//        String url = UploadOSSKit.uploadImage(file);
        String url="";
        Ret ret = Ret.create("state", "SUCCESS")
                .set("url", url)
                .set("path",url)
                .set("original", orig)
                .set("type", "."+typeArr[1])
                .set("size", size);
        renderJson(ret);
    }

}
